import requests
from rauth import OAuth1Service
from rauth import OAuth2Service

from models import User, user_datastore, Role, Organization, OrganizationAPIDocument
from flask_security.confirmable import generate_confirmation_link
from flask_security import Security, logout_user, login_required
from flask_jwt import JWT, jwt_required, current_identity
from sendgrid.helpers.mail import Email, Mail, Content
from flask import render_template, request, redirect
from flask_login import current_user, login_user
from flask_security.utils import verify_password
from sqlalchemy_utils.types.choice import Choice
from werkzeug.utils import secure_filename
from urlparse import urlparse, urljoin
from flask_restless import APIManager
from werkzeug.local import LocalProxy
from application import app, oauth
from flask.json import JSONEncoder
from flask import current_app
from oauth import OAuthSignIn
from admin import init_admin
from sqlalchemy import exc
from urllib import urlencode
# Check in case of performance issues.
from bunch import bunchify
from flask import session
from flask import url_for
from flask import flash
from database import db
import datetime
import secrets
import sendgrid
import flask
import os

# BLUEPRINTS
security = Security(app, user_datastore)


# JWT Token authentication
def authenticate(username, password):
    user = user_datastore.find_user(email=username)
    if user and username == user.email and verify_password(password, user.password):
        return user
    return None


def load_user(payload):
    user = user_datastore.find_user(id=payload['identity'])
    return user


jwt = JWT(app, authenticate, load_user)

_security = LocalProxy(lambda: app.extensions['security'])
_datastore = LocalProxy(lambda: _security.datastore)


# END BLUEPRINTS


# UTILITY FUNCTIONS
def is_safe_url(target):
    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))
    return test_url.scheme in ('http', 'https') and ref_url.netloc == test_url.netloc


def generate_token(user):
    """ Currently this is workaround
    since the latest version that already has this function
    is not published on PyPI yet and we don't want
    to install the package directly from GitHub.
    See: https://github.com/mattupstate/flask-jwt/blob/9f4f3bc8dce9da5dd8a567dfada0854e0cf656ae/flask_jwt/__init__.py#L145
    """
    jwt = current_app.extensions['jwt']
    token = jwt.jwt_encode_callback(user)
    return token


def track(user):
    """
    Function to track user activity in the system.
    :param user:
    :return:
    """
    if 'X-Forwarded-For' in request.headers:
        remote_addr = request.headers.getlist("X-Forwarded-For")[0].rpartition(' ')[-1]
    else:
        remote_addr = request.remote_addr or 'untrackable'

    old_current_login, new_current_login = user.current_login_at, datetime.datetime.utcnow()
    old_current_ip, new_current_ip = user.current_login_ip, remote_addr

    user.last_login_at = old_current_login or new_current_login
    user.current_login_at = new_current_login
    user.last_login_ip = old_current_ip or new_current_ip
    user.current_login_ip = new_current_ip
    user.login_count = user.login_count + 1 if user.login_count else 1


def get_or_create(session, model, **kwargs):
    '''
    Creates an object or returns the object if exists
    credit to Kevin @ StackOverflow
    from: http://stackoverflow.com/questions/2546207/does-sqlalchemy-have-an-equivalent-of-djangos-get-or-create
    '''
    instance = session.query(model).filter_by(**kwargs).first()
    if instance:
        return instance
    else:
        instance = model(**kwargs)
        session.add(instance)
        return instance


github = oauth.remote_app(
    'github',
    consumer_key='51fe28c094d75307a61a',
    consumer_secret='ce7689f6557178eacf9608599d984e0625f3c181',
    request_token_params={'scope': 'user:email'},
    base_url='https://api.github.com/',
    request_token_url=None,
    access_token_method='POST',
    access_token_url='https://github.com/login/oauth/access_token',
    authorize_url='https://github.com/login/oauth/authorize'
)


@github.tokengetter
def get_github_oauth_token():
    return session.get('github_token')


def allowed_file(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in app.config['ALLOWED_EXTENSIONS']


def allowed_avatar(filename):
    return '.' in filename and \
           filename.rsplit('.', 1)[1].lower() in app.config['ALLOWED_AVATARS']


# END UTILITY FUNCTIONS


# CUSTOM ERROR HANDLERS
@app.errorhandler(404)
def not_found_error(error):
    return render_template('404.html'), 404


@app.errorhandler(500)
def internal_error(error):
    db.session.rollback()
    return render_template('500.html'), 500


# END CUSTOM ERROR HANDLERS


# ROUTES AND VIEWS
from flask import Flask, abort, request


@app.route('/handle', methods=["POST"])
def handle():
    data = request.values
    with open("/tmp/out.txt", "w+") as _file:
        _file.write(str(data))
        _file.close()
    return render_template('tester.html', **locals())


@app.route('/')
def home():
    return render_template('index.html')


@app.route('/dashboard')
@login_required
def dashboard():
    return render_template('dashboard.html')


@app.route('/logout')
def log_out():
    logout_user()
    return redirect(request.args.get('next') or '/')


@app.route('/post_confirm_redirect')
def redirect_to_ui():
    """
    Redirects to UI after user confirms email. It adds querystrings
    to inform the ui that the request is a redirect and that it was
    returned after email confirmation.
    :q=redirect
    :confirmation=true
    """
    qs = {'q': 'redirect', 'confirmation': 'true'}
    url = "{0}?{1}".format(app.config["FRONTEND_URL"], urlencode(qs))
    return redirect(url)


# @app.route('/api/v1/callback/github', methods=["GET", "POST", "PUT", "PATCH"])
# def api_authorized_github():
#
#     """
#     Social oauth API callback for Github
#     :return: jwt_token
#     """
#     resp = github.authorized_response()
#     if resp is None or resp.get('access_token') is None:
#         return 'Access denied: reason=%s error=%s resp=%s' % (
#             request.args['error'],
#             request.args['error_description'],
#             resp
#         )
#     session['github_token'] = (resp['access_token'], '')
#     me = github.get('user')
#     social_id = me.data['id']
#     username = me.data['name']
#     email = me.data['email']
#     _social_id = social_id
#     user = User.query.filter_by(social_id="github$" + str(_social_id)).first()
#     role = Role.query.filter_by(name="client").first()
#     exists = db.session.query(db.exists().where(User.social_id == "github$" + str(_social_id))).scalar()
#     if exists is False:
#         email_exists = User.query.filter_by(email=email).first()
#         if email_exists is not None and email is not None:
#             return flask.jsonify({'error': "Email already used."})
#         user = User(social_id="github$" + str(_social_id), username=username, email=email, password=None,
#                     active=True, created=datetime.datetime.now(), roles=[role, ])
#         db.session.add(user)
#         db.session.commit()
#     if user is None:
#         return flask.jsonify({'error': "Authenticaion failed."})
#     login_user(user, True)
#     track(user)
#     db.session.commit()
#     token = generate_token(user)
#
#     return flask.jsonify({'access_token': token})


@app.route('/api/v1/authorize/<provider>', methods=['GET', 'POST', 'OPTIONS'])
def oauth_authorize(provider):
    """
    Initiates social oauth ofr the various providers:

    :param provider: google, facebook, twitter
    :return:
    """
    if provider == "google":
        google_credentials = current_app.config['OAUTH_CREDENTIALS']['google']

        googleinfo = requests.get(
            'https://accounts.google.com/.well-known/openid-configuration').json()
        google = OAuth2Service(
            name='google',
            client_id=google_credentials['id'],
            client_secret=google_credentials['secret'],
            authorize_url=googleinfo.get('authorization_endpoint'),
            base_url=googleinfo.get('userinfo_endpoint'),
            access_token_url=googleinfo.get('token_endpoint')
        )
    if provider == "facebook":
        facebook_credentials = current_app.config['OAUTH_CREDENTIALS']['facebook']
        facebook = OAuth2Service(
            name='facebook',
            client_id=facebook_credentials['id'],
            client_secret=facebook_credentials['secret'],
            authorize_url='https://graph.facebook.com/oauth/authorize',
            access_token_url='https://graph.facebook.com/oauth/access_token',
            base_url='https://graph.facebook.com/'
        )
    if provider == "twitter":
        twitter_credentials = current_app.config['OAUTH_CREDENTIALS']['twitter']
        twitter = OAuth1Service(
            name='twitter',
            consumer_key=twitter_credentials['id'],
            consumer_secret=twitter_credentials['secret'],
            request_token_url='https://api.twitter.com/oauth/request_token',
            authorize_url='https://api.twitter.com/oauth/authorize',
            access_token_url='https://api.twitter.com/oauth/access_token',
            base_url='https://api.twitter.com/1.1/'
        )
    oauth = OAuthSignIn.get_provider(provider)
    return oauth.authorize()


@app.route('/api/v1/callback/<provider>', methods=["GET", "POST", "PUT", "PATCH"])
def api_oauth_callback(provider):
    """
    API callbacks for the various social oauth providers
    :param provider: facebook, github, twitter
    :return: jwt_token
    """
    user = None
    if provider not in ['github']:
        oauth = OAuthSignIn.get_provider(provider)
    if provider == "github":
        resp = github.authorized_response()
        if resp is None or resp.get('access_token') is None:
            return 'Access denied: reason=%s error=%s resp=%s' % (
                request.args['error'],
                request.args['error_description'],
                resp
            )
        session['github_token'] = (resp['access_token'], '')
        me = github.get('user')
        social_id = me.data['id']
        username = me.data['name']
        email = me.data['email']
        _social_id = social_id
        user = User.query.filter_by(social_id="github$" + str(_social_id)).first()
        role = Role.query.filter_by(name="client").first()
        exists = db.session.query(db.exists().where(User.social_id == "github$" + str(_social_id))).scalar()
        if exists is False:
            email_exists = User.query.filter_by(email=email).first()
            if email_exists is not None and email is not None:
                return flask.jsonify({'error': "Email already used."})
            user = User(social_id="github$" + str(_social_id), username=username, email=email, password=None,
                        active=True, created=datetime.datetime.now(), roles=[role, ])
            db.session.add(user)
            db.session.commit()
        if user is None:
            return flask.jsonify({'error': "Authenticaion failed."})
    elif provider == 'google':
        social_id, given_name, email = oauth.callback()
        _social_id = "google${0}".format(str(social_id))
        user = User.query.filter_by(social_id=social_id).first()
        role = Role.query.filter_by(name="client").first()
        if not user:
            user = User(social_id=_social_id, username=given_name + family_name,
                        email=email, password=None, active=True, created=datetime.datetime.now(),
                        roles=[role, ])
            db.session.add(user)
            db.session.commit()
    elif provider == 'twitter':
        social_id, username, email = oauth.callback()
        _social_id = social_id
        user = User.query.filter_by(social_id=social_id).first()
        role = Role.query.filter_by(name="client").first()
        if not user:
            user = User(social_id=_social_id, username=username, email=email,
                        password=None, active=True, created=datetime.datetime.now(), roles=[role, ])
            db.session.add(user)
            db.session.commit()
    elif provider == 'facebook':
        social_id, username, email = oauth.callback()
        _social_id = social_id
        user = User.query.filter_by(social_id=_social_id).first()
        role = Role.query.filter_by(name="client").first()
        if user is None:
            email_exists = User.query.filter_by(email=email).first()
            if email_exists is not None and email is not None:
                return flask.jsonify({'error': "Email already used."})
            user = User(social_id=_social_id, username=username, email=email,
                        password=None, active=True, created=datetime.datetime.now(), roles=[role, ])
            db.session.add(user)
            db.session.commit()
    if user is None:
        return flask.jsonify({'error': "Authenticaion failed."})
    login_user(user, True)
    track(user)
    db.session.commit()
    token = generate_token(user)
    return flask.jsonify({'access_token': token})


@app.route('/upload_avatar', methods=['GET', 'POST'])
def upload_avatar():
    if current_user.is_authenticated:
        if request.method == 'POST':
            if 'file' not in request.files:
                flash('No file part')
                # return redirect(request.url)
                return flask.jsonify({"error": "No file in request"})
            file = request.files['file']
            if file.filename == '':
                flash('No selected file')
                # return redirect(request.url)
                return flask.jsonify({"error": "No file selected"})
            if file and allowed_avatar(file.filename):
                filename = secure_filename(file.filename)
                file.save(os.path.join(app.config['AVATARS_FOLDER'], filename))
                try:
                    current_identity.photo = filename
                except AttributeError:
                    current_user.photo = filename
                db.session.commit()
                return flask.jsonify({"success": filename})

        return '''
        <!doctype html>
        <title>Upload avatar</title>
        <h1>Upload avatar.</h1>
        <form method=post enctype=multipart/form-data>
          <p><input type=file name=file>
             <input type=submit value=Upload>
        </form>
        '''
    else:
        return redirect(url_for('login.index'))


@app.route('/upload_national_id_front', methods=['GET', 'POST'])
def upload_national_id_front():
    if current_user.is_authenticated:
        if current_user.organization_id:
            if request.method == 'POST':
                if 'file' not in request.files:
                    flash('No file part')
                    # return redirect(request.url)
                    return flask.jsonify({"error": "No file in request"})
                file = request.files['file']
                if file.filename == '':
                    flash('No selected file')
                    # return redirect(request.url)
                    return flask.jsonify({"error": "No file selected"})
                if file and allowed_file(file.filename):
                    filename = secure_filename(file.filename)
                    file.save(os.path.join(app.config['ID_FRONT_UPLOAD_FOLDER'], filename))
                    try:
                        organization_id = Organization.query.join(User).filter(
                            Organization.users.contains(current_identity)).with_entities(
                            Organization.id)
                    except AttributeError:
                        try:
                            organization_id = Organization.query.join(User).filter(
                                Organization.users.contains(current_user)).with_entities(
                                Organization.id)
                        except AttributeError:
                            return flask.jsonify({"error": "You have to be logged in to upload."})
                    try:
                        new_api_documents_id_front = OrganizationAPIDocument(organization_id=organization_id,
                                                                             national_id_front=filename)
                        db.session.add(new_api_documents_id_front)
                        db.session.commit()
                    except exc.IntegrityError:
                        api_document_record = OrganizationAPIDocument.query.join(Organization).filter(
                            organization_id=organization_id)
                        api_document_record.national_id_front = filename
                        db.session.commit()
                    # return redirect(url_for('uploaded_file',
                    #                        filename=filename))
                    return flask.jsonify({"success": filename})

            return '''
            <!doctype html>
            <title>Upload national id front</title>
            <h1>Upload National ID front.</h1>
            <form method=post enctype=multipart/form-data>
              <p><input type=file name=file>
                 <input type=submit value=Upload>
            </form>
            '''
        else:
            return flask.jsonify({"error": "You must belong to an organization to upload a document."})
    else:
        return redirect(url_for('login.index'))


@app.route('/upload_national_id_back', methods=['GET', 'POST'])
def upload_national_id_back():
    if current_user.is_authenticated:
        if current_user.organization_id:
            if request.method == 'POST':
                if 'file' not in request.files:
                    flash('No file part')
                    # return redirect(request.url)
                    return flask.jsonify({"error": "No file in request"})
                file = request.files['file']
                if file.filename == '':
                    flash('No selected file')
                    # return redirect(request.url)
                    return flask.jsonify({"error": "No file selected"})
                if file and allowed_file(file.filename):
                    filename = secure_filename(file.filename)
                    file.save(os.path.join(app.config['ID_BACK_UPLOAD_FOLDER'], filename))
                    try:
                        organization_id = Organization.query.join(User).filter(
                            Organization.users.contains(current_identity)).with_entities(
                            Organization.id)
                    except AttributeError:
                        try:
                            organization_id = Organization.query.join(User).filter(
                                Organization.users.contains(current_user)).with_entities(
                                Organization.id)
                        except AttributeError:
                            return flask.jsonify({"error": "You have to be logged in to upload."})
                    try:
                        new_api_documents_id_front = OrganizationAPIDocument(organization_id=organization_id,
                                                                             national_id_back=filename)
                        db.session.add(new_api_documents_id_front)
                        db.session.commit()
                    except exc.IntegrityError:
                        api_document_record = OrganizationAPIDocument.query.join(Organization).filter(
                            organization_id=organization_id)
                        api_document_record.national_id_back = filename
                        db.session.commit()
                    # return redirect(url_for('uploaded_file',
                    #                        filename=filename))
                    return flask.jsonify({"success": filename})

            return '''
            <!doctype html>
            <title>Upload national id back</title>
            <h1>Upload National ID back.</h1>
            <form method=post enctype=multipart/form-data>
              <p><input type=file name=file>
                 <input type=submit value=Upload>
            </form>
            '''
        else:
            return flask.jsonify({"error": "You must belong to an organization to upload a document."})
    else:
        return redirect(url_for('login.index'))


@app.route('/upload_directors_letter', methods=['GET', 'POST'])
def upload_directors_letter():
    if current_user.is_authenticated:
        if current_user.organization_id:
            if request.method == 'POST':
                if 'file' not in request.files:
                    flash('No file part')
                    # return redirect(request.url)
                    return flask.jsonify({"error": "No file in request"})
                file = request.files['file']
                if file.filename == '':
                    flash('No selected file')
                    # return redirect(request.url)
                    return flask.jsonify({"error": "No file selected"})
                if file and allowed_file(file.filename):
                    filename = secure_filename(file.filename)
                    file.save(os.path.join(app.config['ID_BACK_UPLOAD_FOLDER'], filename))
                    try:
                        organization_id = Organization.query.join(User).filter(
                            Organization.users.contains(current_identity)).with_entities(
                            Organization.id)
                    except AttributeError:
                        organization_id = Organization.query.join(User).filter(
                            Organization.users.contains(current_user)).with_entities(
                            Organization.id)
                    # finally:
                    #    return flask.jsonify({"error": "You have to be logged in to upload."})
                    try:
                        new_api_documents_id_front = OrganizationAPIDocument(organization_id=organization_id,
                                                                             directors_letter=filename)
                        db.session.add(new_api_documents_id_front)
                        db.session.commit()
                    except exc.IntegrityError:
                        api_document_record = OrganizationAPIDocument.query.join(Organization).filter(
                            organization_id=organization_id)
                        api_document_record.directors_letter = filename
                        db.session.commit()
                    # return redirect(url_for('uploaded_file',
                    #                        filename=filename))
                    return flask.jsonify({"success": filename})

            return '''
            <!doctype html>
            <title>Upload directors letter</title>
            <h1>Upload directors letter.</h1>
            <form method=post enctype=multipart/form-data>
              <p><input type=file name=file>
                 <input type=submit value=Upload>
            </form>
            '''
        else:
            return flask.jsonify({"error": "You must belong to an organization to upload a document."})
    else:
        return redirect(url_for('login.index'))


@app.route('/upload_certificate_of_incorporation', methods=['GET', 'POST'])
def upload_certificate_of_incorporation():
    # organisation id should be in post data
    if current_user.is_authenticated:
        if current_user.organization_id:
            if request.method == 'POST':
                if 'file' not in request.files:
                    flash('No file part')
                    # return redirect(request.url)
                    return flask.jsonify({"error": "No file in request"})
                file = request.files['file']
                if file.filename == '':
                    flash('No selected file')
                    # return redirect(request.url)
                    return flask.jsonify({"error": "No file selected"})
                if file and allowed_file(file.filename):
                    filename = secure_filename(file.filename)
                    file.save(os.path.join(app.config['ID_BACK_UPLOAD_FOLDER'], filename))
                    try:
                        organization_id = Organization.query.join(User).filter(
                            Organization.users.contains(current_identity)).with_entities(
                            Organization.id)
                    except AttributeError:
                        organization_id = Organization.query.join(User).filter(
                            Organization.users.contains(current_user)).with_entities(
                            Organization.id)
                    # finally:
                    #    return flask.jsonify({"error": "You have to be logged in to upload."})
                    try:
                        new_api_documents_id_front = OrganizationAPIDocument(organization_id=organization_id,
                                                                             certificate_of_incorporation=filename)
                        db.session.add(new_api_documents_id_front)
                        db.session.commit()
                    except exc.IntegrityError:
                        api_document_record = OrganizationAPIDocument.query.join(Organization).filter(
                            organization_id=organization_id)
                        api_document_record.certificate_of_incorporation = filename
                        db.session.commit()
                    # return redirect(url_for('uploaded_file',
                    #                        filename=filename))
                    return flask.jsonify({"success": filename})

            return '''
            <!doctype html>
            <title>Upload certificate of incorporation</title>
            <h1>Upload certificate of incorporation.</h1>
            <form method=post enctype=multipart/form-data>
              <p><input type=file name=file>
                 <input type=submit value=Upload>
            </form>
            '''
        else:
            return flask.jsonify({"error": "You must belong to an organization to upload a document."})
    else:
        return redirect(url_for('login.index'))


@app.route('/upload_pin_certificate', methods=['GET', 'POST'])
def upload_pin_certificate():
    if current_user.is_authenticated:
        if current_user.organization_id:
            if request.method == 'POST':
                if 'file' not in request.files:
                    flash('No file part')
                    # return redirect(request.url)
                    return flask.jsonify({"error": "No file in request"})
                file = request.files['file']
                if file.filename == '':
                    flash('No selected file')
                    # return redirect(request.url)
                    return flask.jsonify({"error": "No file selected"})
                if file and allowed_file(file.filename):
                    filename = secure_filename(file.filename)
                    file.save(os.path.join(app.config['ID_BACK_UPLOAD_FOLDER'], filename))
                    try:
                        organization_id = Organization.query.join(User).filter(
                            Organization.users.contains(current_identity)).with_entities(
                            Organization.id)
                    except AttributeError:
                        organization_id = Organization.query.join(User).filter(
                            Organization.users.contains(current_user)).with_entities(
                            Organization.id)
                    # finally:
                    #    return flask.jsonify({"error": "You have to be logged in to upload."})
                    try:
                        new_api_documents_id_front = OrganizationAPIDocument(organization_id=organization_id,
                                                                             pin_certificate=filename)
                        db.session.add(new_api_documents_id_front)
                        db.session.commit()
                    except exc.IntegrityError:
                        api_document_record = OrganizationAPIDocument.query.join(Organization).filter(
                            organization_id=organization_id)
                        api_document_record.pin_certificate = filename
                        db.session.commit()
                    # return redirect(url_for('uploaded_file',
                    #                        filename=filename))
                    return flask.jsonify({"success": filename})

            return '''
            <!doctype html>
            <title>Upload pin certificate</title>
            <h1>Upload pin certificate.</h1>
            <form method=post enctype=multipart/form-data>
              <p><input type=file name=file>
                 <input type=submit value=Upload>
            </form>
            '''
        else:
            return flask.jsonify({"error": "You must belong to an organization to upload a document."})
    else:
        return redirect(url_for('login.index'))

# END ROUTES and VIEWS

# API POST-PROCESSORS


def send_confirmation_email(result, **kwargs):
    """
    Sends introductory email to user. The email contains the confirmation link
    :param result:
    :param kwargs:
    :return:
    """
    if _security.confirmable:
        data = bunchify(result)
        confirmation_link, token = generate_confirmation_link(data)
        sg = sendgrid.SendGridAPIClient(apikey=app.config["SENDGRID_API_KEY"])
        from_email = Email(app.config["SENDGRID_DEFAULT_FROM"])
        subject = "Welcome to Paycast!"
        to_email = Email(data.email)
        content = Content("text/plain", confirmation_link)
        mail = Mail(from_email, subject, to_email, content)
        try:
            response = sg.client.mail.send.post(request_body=mail.get())
        except Exception:
            return {"error": "Error sending email."}

        assert data.id is not None


# END API POST-PROCESSORS


# API PRE-PROCESSORS
def add_user_role(data=None, **kwargs):
    del data["password_confirm"]
    client_role = get_or_create(db.session, Role,
                                name="client",
                                description="Client user(user who signed up using oauth or email + password)."
                                )
    if data:
        data['roles'] = [client_role]
    return data


@jwt_required()
def key_and_secret_generator(data=None, **kwargs):
    test_key = secrets.token_urlsafe(32)
    test_secret = secrets.token_urlsafe(32)
    live_key = secrets.token_urlsafe(32)
    live_secret = secrets.token_urlsafe(32)
    if data:
        data['test_secret_key'] = test_key
        data['test_public_key'] = test_secret
        data['live_public_key'] = live_key
        data['live_secret_key'] = live_secret
    return data


@jwt_required()
def auth_check_func(*args, **kwargs):
    """
    Checks for token in request.
    :param args:
    :param kwargs:
    :return:
    """
    pass


@jwt_required()
def filter_func(instance_id=None, **kw):
    instance_id = current_identity.id
    return instance_id


@jwt_required()
def add_creator(result=None, **kw):
    organization = Organization.query.get(result['id'])
    current_identity.organizations.append(organization)
    db.session.commit()
    return result


@jwt_required()
def organization_filter_func(instance_id=None, **kw):
    instance_id = Organization.query.filter(Organization.users.contains(current_identity)).with_entities(
        Organization.id)
    return instance_id

# END PRE-PROCESSORS


        # from Crypto.Hash import HMAC, SHA256
        # import string
        # from base64 import b64encode
        # Secret key is user specific
        # Create signed message. Do this to data sent to user.
        # string_to_sign = string.encode('utf-8')
        # _hmac = HMAC.new('secret-shared-key-goes-here', string_to_sign, SHA256)
        # digest = b64encode(_hmac.hexdigest())

        # Route for client library
        # @app.route('/api/v1/payment/c2b', METHODS=['POST'])
        # def make_payment():
        # Request should come with a base64 url safe encoded header of the HMAC e.g "HMAC: UKW-EaC9diBPuRTgwaUprw4pf4h1nTJyClCT48dbhQo"
        # Request POST should also have client key. This will be used for identification of the client.
        # Request POST will also have the data payload
        # What parameters do the payment service expect?


# API ENDPOINTS

apimanager = APIManager(app, flask_sqlalchemy_db=db)
apimanager.create_api(User,
                      methods=['POST'],
                      url_prefix='/api/v1',
                      preprocessors=dict(POST=[add_user_role]),
                      postprocessors=dict(POST=[send_confirmation_email]),
                      collection_name='users',
                      exclude_columns=['password'])

apimanager.create_api(User,
                      methods=['PUT'],
                      url_prefix='/api/v1',
                      preprocessors=dict(POST=[auth_check_func], GET_SINGLE=[auth_check_func],
                                         GET_MANY=[auth_check_func], PUT=[key_and_secret_generator]),
                      collection_name='users',
                      exclude_columns=['password']
                      )

apimanager.create_api(User,
                      methods=['GET'],
                      url_prefix='/api/v1',
                      preprocessors=dict(GET=[auth_check_func], GET_SINGLE=[auth_check_func, filter_func]),
                      collection_name='users',
                      exclude_columns=['password']
                      )

apimanager.create_api(Organization,
                      methods=['GET'],
                      url_prefix='/api/v1',
                      preprocessors=dict(GET=[auth_check_func, organization_filter_func],
                                         GET_SINGLE=[auth_check_func, organization_filter_func],
                                         GET_MANY=[auth_check_func, organization_filter_func]),
                      collection_name='organizations',
                      )

apimanager.create_api(Organization,
                      methods=['POST'],
                      url_prefix='/api/v1',
                      preprocessors=dict(POST=[auth_check_func, key_and_secret_generator]),
                      postprocessors=dict(POST=[add_creator]),
                      collection_name='organizations',
                      )

apimanager.create_api(Organization,
                      methods=['PUT'],
                      url_prefix='/api/v1',
                      preprocessors=dict(PUT=[auth_check_func]),
                      collection_name='organizations',
                      )

apimanager.create_api(OrganizationAPIDocument,
                      methods=['GET'],
                      url_prefix='/api/v1',
                      preprocessors=dict(POST=[auth_check_func]),
                      collection_name='organizations',
                      )

apimanager.create_api(OrganizationAPIDocument,
                      methods=['POST'],
                      url_prefix='/api/v1',
                      preprocessors=dict(POST=[auth_check_func]),
                      collection_name='organizations',
                      )

apimanager.create_api(OrganizationAPIDocument,
                      methods=['PUT'],
                      url_prefix='/api/v1',
                      preprocessors=dict(PUT=[auth_check_func]),
                      collection_name='organizations',
                      )

# END API ENDPOINTS


class ChoiceJSONEncoder(JSONEncoder):
    def default(self, obj):
        try:
            if isinstance(obj, Choice):
                choices = {"value": obj.value}
                return choices
            iterable = iter(obj)
        except TypeError:
            pass
        else:
            return list(iterable)
        return JSONEncoder.default(self, obj)


app.json_encoder = ChoiceJSONEncoder

# Setup Admin
init_admin()


# Create organization users list endpoint
# Return users' role, email and date created

# Create endpoint for pulling payment details from James' billing module.


# Bootstrap
def create_test_models():
    admin_role = user_datastore.create_role(
        name="admin",
    )
    user_datastore.create_role(
        name="client",
    )
    user_datastore.create_user(email='arlus@faidika.co.ke', password='testpassword', active=True,
                               confirmed_at=datetime.datetime.now(), roles=[admin_role, ])

    db.session.commit()


# To allow Cross-Origin Resource Sharing (CORS)
@app.after_request
def after_request(response):
    response.headers.add('Access-Control-Allow-Origin', '*')
    if request.method == 'OPTIONS':
        response.headers['Access-Control-Allow-Methods'] = 'DELETE, GET, POST, PUT'
        headers = request.headers.get('Access-Control-Request-Headers')
        if headers:
            response.headers['Access-Control-Allow-Headers'] = headers
    return response


db.init_app(app)
with app.app_context():
    db.create_all()

# Start server
if __name__ == '__main__':
    app.run()
