from flask import Flask
from flask_oauthlib.client import OAuth
from opbeat.contrib.flask import Opbeat

app = Flask(__name__)
app.config.from_object('config.DevelopmentConfig')
oauth = OAuth(app)
opbeat = Opbeat(
    app,
    organization_id='0f284cab6fe043318cb4e6dab386dd51',
    app_id='1fbd7aa8fb',
    secret_token='218f81c050a52480a960be6e60dc4cd1e10d51b5',
)


