from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand
import datetime
from sqlalchemy import exc
from flask import Flask
#from flask_mail import Mail
from flask_sendgrid import SendGrid
from flask_oauthlib.client import OAuth
from flask_security import Security
from models import User, user_datastore, Role, db

app = Flask(__name__)
app.config.from_object('config.DevelopmentConfig')
#mail = Mail(app)
mail = SendGrid(app)
oauth = OAuth(app)
security = Security(app, user_datastore)

migrate = Migrate(app, db)

manager = Manager(app)
manager.add_command('db', MigrateCommand)


def get_or_create(session, model, **kwargs):
    '''
    Creates an object or returns the object if exists
    credit to Kevin @ StackOverflow
    from: http://stackoverflow.com/questions/2546207/does-sqlalchemy-have-an-equivalent-of-djangos-get-or-create
    '''
    instance = session.query(model).filter_by(**kwargs).first()
    if instance:
        return instance
    else:
        instance = model(**kwargs)
        session.add(instance)
        return instance


@manager.command
def create_admin():
    """Creates the administrative user."""
    db.init_app(app)
    db.create_all()
    new_role = get_or_create(db.session, Role,
                             name="admin",
                             description="Administrative user."
                             )
    get_or_create(db.session, Role,
                  name="client",
                  description="Client user(user who signed up using oauth or email + password)."
                  )
    db.session.commit()

    try:
        User(
            email="arlus@faidika.co.ke",
            password="adminpass",
            username="arlus",
            full_name="Aurlus Ismael",
            active=True,
            created=datetime.datetime.now(),
            roles=[new_role, ],
            social_id=None,
            confirmed_at=datetime.datetime.now()
        )
        db.session.commit()
    except exc.IntegrityError:
        db.session().rollback()


if __name__ == '__main__':
    manager.run()
