from flask_admin.contrib.sqla import ModelView
from flask_admin import Admin, BaseView, expose
from flask_security import current_user
from flask import redirect
from flask_security import logout_user
from flask_admin.contrib.fileadmin import FileAdmin

from application import app
from database import db
from models import User, Role, Organization
import os.path as op

path = op.join(op.dirname(__file__), 'static')


class LogoutView(BaseView):
    @expose('/')
    def index(self):
        logout_user()
        return redirect('/admin')

    def is_visible(self):
        return current_user.is_authenticated


class LoginView(BaseView):
    @expose('/')
    def index(self):
        logout_user()
        return redirect('/login?next=/admin')

    def is_visible(self):
        return not current_user.is_authenticated


class AdminModelView(ModelView):
    def is_accessible(self):
        return current_user.is_authenticated


class UserModelView(AdminModelView):
    column_list = ('email', 'username', 'social_id', 'active', 'last_login_at', 'roles', 'organizations')


class OrganizationModelView(AdminModelView):
    column_display_pk = True  # optional, but I like to see the IDs in the list
    column_hide_backrefs = False
    #column_list = ('id', 'name', 'users')


def init_admin():
    admin = Admin(app)
    admin.add_view(UserModelView(User, db.session, category='Authentication'))
    admin.add_view(AdminModelView(Role, db.session, category='Authentication'))
    admin.add_view(OrganizationModelView(Organization, db.session, category='Clients'))
    admin.add_view(FileAdmin(path, '/static', name='Static Files'))
    admin.add_view(LogoutView(name='Logout', endpoint='logout'))
    admin.add_view(LoginView(name='Login', endpoint='login'))
