from database import db
from sqlalchemy.orm import validates, backref
from flask_security import UserMixin, RoleMixin, SQLAlchemyUserDatastore
from flask_security.utils import encrypt_password
from sqlalchemy_utils.types.choice import ChoiceType

roles_users = db.Table('roles_users',
                       db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
                       db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))

user_organization_relationship_table = db.Table('relationship_table',
                                                db.Column('user_id', db.Integer, db.ForeignKey('user.id'),
                                                          nullable=False),
                                                db.Column('organization_id', db.Integer,
                                                          db.ForeignKey('organization.id'), nullable=False),
                                                db.PrimaryKeyConstraint('user_id', 'organization_id'))


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    username = db.Column(db.String(100))
    full_name = db.Column(db.String(100), nullable=True)
    social_id = db.Column(db.String(64), unique=True, nullable=True)
    password = db.Column(db.String(255), nullable=True)
    active = db.Column(db.Boolean(), default=True)
    confirmed_at = db.Column(db.DateTime())
    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))
    created = db.Column(db.DateTime)
    last_login_at = db.Column(db.DateTime())
    current_login_at = db.Column(db.DateTime())
    last_login_ip = db.Column(db.String(255))
    current_login_ip = db.Column(db.String(255))
    login_count = db.Column(db.Integer)
    photo = db.Column(db.String(255), nullable=True)
    phone_number = db.Column(db.String(255), nullable=True)
    #organization_id = db.Column("Organization", db.ForeignKey('organization.id'))
    #organization = db.relationship("Organization", back_populates='users')
    organizations = db.relationship("Organization", secondary=user_organization_relationship_table, backref='users')

    def __repr__(self):
        return '<models.User[email=%s]>' % self.email

    @validates('password')
    def validate_and_encrypt_password(self, key, password):
        return encrypt_password(password)


class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))


user_datastore = SQLAlchemyUserDatastore(db, User, Role)


class Organization(db.Model):
    """FUTURE TODO: ENABLE SEVERAL SERVICES UNDER AN ORGANIZATION
    """
    STATUS = [
        (u'ACTIVE', u'ACTIVE'),
        (u'DEACTIVATED', u'DEACTIVATED')
    ]
    BILLING_MODES = [
        (u'PERCENTAGE', u'PERCENTAGE'),
        (u'AMOUNT', u'AMOUNT')
    ]
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(80), unique=True)
    logo = db.Column(db.String(255), nullable=True)
    test_secret_key = db.Column(db.Text, nullable=True)
    test_public_key = db.Column(db.Text, nullable=True)
    live_secret_key = db.Column(db.Text, nullable=True)
    live_public_key = db.Column(db.Text, nullable=True)
    test_callback_url = db.Column(db.String(255), nullable=True)
    live_callback_url = db.Column(db.String(255), nullable=True)
    billing_mode = db.Column(ChoiceType(BILLING_MODES, impl=db.String()), default=u'PERCENTAGE')
    billing = db.Column(db.Float(), nullable=True)
    billing_mode_effective_date = db.Column(db.DateTime, default=db.func.current_timestamp())
    keys_generation_datetime = db.Column(db.DateTime, default=db.func.current_timestamp())
    status = db.Column(ChoiceType(STATUS, impl=db.String()), default=u'ACTIVE')

    def __repr__(self):
        try:
            return '<Organization %r>' % self.name
        except AttributeError:
            return ""


class OrganizationAPIDocument(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    organization_id = db.Column(db.Integer, db.ForeignKey('organization.id'))
    organization = db.relationship('Organization', backref='organization')
    national_id_front = db.Column(db.String(255), nullable=True)
    national_id_back = db.Column(db.String(255), nullable=True)
    national_id_front_verified = db.Column(db.Boolean(), default=False)
    national_id_back_verified = db.Column(db.Boolean(), default=False)
    directors_letter = db.Column(db.String(255), nullable=True)
    directors_letter_verified = db.Column(db.Boolean(), default=False)
    certificate_of_incorporation = db.Column(db.String(255), nullable=True)
    certificate_of_incorporation_verified = db.Column(db.Boolean(), default=False)
    pin_certificate = db.Column(db.String(255), nullable=True)
    pin_certificate_verified = db.Column(db.Boolean(), default=False)

    def __repr__(self):
        try:
            return '<Organization %r>' % self.organization.name
        except AttributeError:
            return ""
