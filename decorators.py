from functools import wraps
from flask import request, abort
from application import app
from base64 import b64encode
from datetime import datetime
from Crypto.Hash import SHA256, HMAC


# The actual decorator function
def internal_billing_require_app_key(view_function):
    @wraps(view_function)
    # the new, post-decoration function. Note *args and **kwargs here.
    def decorated_function(*args, **kwargs):
        # Requests from other service should be as follows:
        # method: POST
        # headers: api_key - required
        # data: hmac
        if request.headers.get('api_key') == app.config['BILLING_API_KEY']:
            return view_function(*args, **kwargs)
        else:
            # return {"error": "invalid api key"}
            abort(401)

    return decorated_function


def internal_transaction_require_app_key(view_function):
    @wraps(view_function)
    # the new, post-decoration function. Note *args and **kwargs here.
    def decorated_function(*args, **kwargs):
        # Requests from other service should be as follows:
        # method: POST
        # headers: api_key - required
        # data: hmac
        if request.headers.get('api_key') == app.config['TRANSACTION_API_KEY']:
            return view_function(*args, **kwargs)
        else:
            # return {"error": "invalid api key"}
            abort(401)

    return decorated_function


# Usage
# @internal_billing_require_app_key

from sys import argv


def create_signature(secret_key, string):
    """ Create the base64 encoded signed message from api_key and string_to_sign"""
    string_to_sign = string.encode('utf-8')
    hmac = HMAC.new(secret_key, string_to_sign, SHA256)
    return b64encode(hmac.hexdigest())


def create_token(access_key):
    string_to_sign = "POST\n" + \
                     "application/x-www-form-urlencoded\n" + \
                     datetime.utcnow().strftime("%Y-%m-%dT%H:%M")
    # Lookup secret_key
    user_secret_key = ""
    hmac = create_signature(user_secret_key, string_to_sign)
    signature = "AUTH:" + access_key + ":" + hmac
    return signature


def authenticate_signed_token(auth_token):
    """ Take token, recreate signature, auth if a match """
    lead, access_key, signature = auth_token.split(":")
    if lead.upper() == "AUTH":
        our_token = create_token(access_key).split(":", 1)[-1]
    return True if signature == our_token else False


if __name__ == "__main__":
    print create_token('secret_api_key')
    print authenticate_signed_token(argv[1])

