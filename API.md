***Paycast-Auth API***
----
Strictly HTTPS 

**URL**: https://api.paycast.io

**API prefix**: /api/v1

**1. Authenticate user (username+password)**
* **URL:**
    /auth
* **Method:**
    `POST`
* **URL Params:**

  **Required:**
  None
  
  **Optional:**
  None
  
* **Headers:**
  None
  
* **Data Params:**

  **Required:**
  
  `username` - User's email address
  
  `password` - User's password
  
  e.g {"username":"user@email.com","password":"password"}
  
* **Success Response:**

  * **status_code:** 200
  
    **Content:** `{ 'access_token' : "token" }`
 
* **Error Response:**

  * **status_code:** 401 
  
    **Content:** `{ 'description' : "Invalid credentials", 'error': "Bad Request", 'status_code': 401 }`
    
**2. Authenticate user (social auth)**
* **URL(Twitter):**
    /authorize/twitter
    
    This directly triggers the backend directly for authentication. The client
    is not involved in the flow since Twitter doesn't yet support CORS headers.
    https://twittercommunity.com/t/will-twitter-api-support-cors-headers-soon/28276/4
    
    
    
* **URL(Github, Facebook, Google):**
    /callback/:provider
    
* **Method:**
    `POST`
* **URL Params:**

  **Required:**
  
  `provider`: google, facebook, github
  
  **Optional:**
  None
  
* **Headers:**
  None
  
* **Data Params:**

  **Required:**
  
  None
  
* **Success Response:**

  * **status_code:** 200
  
    **Content:** `{ 'access_token' : "token" }`
 
* **Error Response:**

  * **status_code:** 401
  
    **Content:** 
        
    
**3. Get current user**
* **URL:**
    /users/:id
* **Method:**
    `GET`
* **URL Params:**

  **Required:**
   `id=[integer]`
  
  **Optional:**
  None
  
* **Headers:**

  `Authorization`: JWT jwt_token_received_from_auth

* **Data Params:**
  None

* **Success Response:**

  * **status_code:** 200
  
    **Content:** `{
  "email": "user@email.com", 
  "id": 1, 
  "social_id": null, 
  "username": "username"
}`
 
* **Error Response:**

  * **status_code:** 401
  
    **Content:** `{
  "description": "Token missing", 
  "error": "Invalid JWT header", 
  "status_code": 401
}`

**4. Create Organization**
* **URL:**
    /organizations
* **Method:**
    `POST`
* **URL Params:**

  **Required:**
  None
  
  **Optional:**
  None
  
* **Headers:**

  `Authorization`: JWT jwt_token_received_from_auth

* **Data Params:**

  **Required:**
  
  `name` - Organization's name
  
  e.g {"name":"First Sample Organization"}

* **Success Response:**

  * **status_code:** 200
  
    **Content:** `{
  "id": 1, 
  "name": "First Sample Organization"
}`
 
* **Error Response:**

  * **status_code:** 401
  
    **Content:** `{
  "message": "IntegrityError"
}`

**5. Current user's Organization**
* **URL:**
    /organizations/:id
* **Method:**
    `GET`
* **URL Params:**

  **Required:**
  `id=[integer]`
  
  **Optional:**
  None
  
* **Headers:**

  `Authorization`: JWT jwt_token_received_from_auth

* **Data Params:**
    None

* **Success Response:**

  * **status_code:** 200 
  
    **Content:** `{
  "billing": null, 
  "billing_mode": {
    "value": "PERCENTAGE"
  }, 
  "billing_mode_effective_date": "2017-03-02T11:37:06.221814", 
  "id": 1, 
  "keys_generation_datetime": "2017-03-02T11:37:06.221814", 
  "live_callback_url": null, 
  "live_public_key": "", 
  "live_secret_key": "" 
  "logo": null, 
  "name": "Final Sample Organization", 
  "organization": [], 
  "status": {
    "value": "ACTIVE"
  }, 
  "test_callback_url": null, 
  "test_public_key": "", 
  "test_secret_key": "" 
  "users": [
    {
      "active": true, 
      "confirmed_at": "2017-03-01T12:39:41.109114", 
      "created": "2017-03-01T12:39:41.109091", 
      "current_login_at": "2017-03-07T08:01:49.327952", 
      "current_login_ip": "154.70.39.29", 
      "email": "arlus@faidika.co.ke", 
      "full_name": "Aurlus Ismael", 
      "id": 1, 
      "last_login_at": "2017-03-07T06:35:02.041419", 
      "last_login_ip": "154.70.39.29", 
      "login_count": 3, 
      "password": "$pbkdf2-sha512$25000$NMYYAyAkhDDGmBOCMMYY4w$U9A73tw5zR7Vc6hvPA4ZuxZfBFF.EBZSJ./IzdJQ.lJCN.7mm3.wB3BNFEkbzH3alTwJ5re.u.Q.n8tHEp4pFA", 
      "phone_number": null, 
      "photo": null, 
      "social_id": null, 
      "username": "arlus"
    }
  ]
}`
 
* **Error Response:**

  * **status_code:** 404 
  
    **Content:** `{}`
    
**6. Add user to organization**
* **URL:**
    /organizations/:id
* **Method:**
    `PUT`
* **URL Params:**

  **Required:**
  `id=[integer]`
  
  **Optional:**
  None
  
* **Headers:**

  `Authorization`: JWT jwt_token_received_from_auth

* **Data Params:**

  `organizations`: List of the organization the user is to be added to
  
  e,g  {"email":"user@email.com"}

* **Success Response:**

  * **status_code:** 200 
  
    **Content:** ``
 
* **Error Response:**

  * **status_code:** 404 
  
    **Content:** `{}`