import os
from datetime import timedelta


class Config(object):
    DEBUG = False
    TESTING = False
    SQLALCHEMY_DATABASE_URI = ''


class ProductionConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'postgresql://paycast:l0v3c0d1ng@localhost/paycast'
    DEBUG = True
    APP_NAME = 'Paycast'
    SECRET_KEY = os.urandom(10)
    JWT_EXPIRATION_DELTA = timedelta(days=30)
    JWT_AUTH_URL_RULE = '/api/v1/auth'
    JWT_AUTH_HEADER_PREFIX = 'JWT'
    SECURITY_REGISTERABLE = True
    SECURITY_REGISTER_URL = '/create_account'
    SECURITY_RECOVERABLE = True
    SECURITY_TRACKABLE = True
    SECURITY_CONFIRMABLE = True
    SECURITY_PASSWORD_HASH = 'pbkdf2_sha512'
    SECURITY_PASSWORD_SALT = 'RgDUQTs4nzQAQRJf9kevRgDUQTs4nzQAQRJf9kev'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECURITY_EMAIL_SENDER = 'no-reply@localhost'
    SECURITY_POST_LOGIN_VIEW = '/dashboard'
    SECURITY_POST_CONFIRM_VIEW = '/post_confirm_redirect'
    SECURITY_POST_RESET_VIEW = '/login'
    FRONTEND_URL = "http://paycast.io/"
    OAUTH_CREDENTIALS = {
        'facebook': {
            'id': '185436661892522',
            'secret': '82a09d542035ee5f10360846b706cb4a'
        },
        'twitter': {
            'id': 'sGHuFe9gdbcc9hzKYkENt3Je6',
            'secret': 'etGxvdXGegdyO1HFo2XfONEDYNK76QREMcPSHsH7n8l7j2umvJ'
        },
        'google': {
            'id': '552817426609-6rfk2dto43877j0rsa0bnh9d1o1m6esi.apps.googleusercontent.com',
            'secret': 'AwVHLZ6XFw9bMwp-diDJ1_wT'
        },
        'github': {
            'id': '51fe28c094d75307a61a',
            'secret': 'ce7689f6557178eacf9608599d984e0625f3c181'
        }
    }
    LANGUAGES = {
        'en': 'English',
    }
    UPLOADS_FOLDER = os.path.join(os.path.dirname(os.path.dirname(__file__)), "uploaded_documents")
    ID_FRONT_UPLOAD_FOLDER = os.path.join(os.path.dirname(os.path.abspath(__file__)), "uploaded_documents/id_front")
    ID_BACK_UPLOAD_FOLDER = os.path.join(os.path.dirname(os.path.abspath(__file__)), "uploaded_documents/id_back")
    CERTIFICATE_OF_INCORPORATION_UPLOAD_FOLDER = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                                              "uploaded_documents/incorporation_certificate")
    DIRECTORS_LETTER_UPLOAD_FOLDER = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                                  "uploaded_documents/directors_letter")
    PIN_CERTIFICATE_UPLOAD_FOLDER = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                                 "uploaded_documents/pin_certificate")
    AVATARS_FOLDER = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                  "uploaded_documents/avatars")

    ALLOWED_EXTENSIONS = set(['pdf', 'png', 'jpg', 'jpeg', 'gif'])
    ALLOWED_AVATARS = set(['png', 'jpg', 'jpeg', 'gif'])
    SENDGRID_API_KEY = "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"
    SENDGRID_DEFAULT_FROM = "admin@paycsat.tech"

    # Redirect to Dennis' login page?
    SECURITY_POST_RESET_VIEW = ''

    # After password change?
    SECURITY_POST_CHANGE_VIEW = ''

    # API configuration
    INTERNAL_API_KEY = ""
    INTERNAL_API_SECRET = ""
    BILLING_API_KEY = ""
    BILLING_API_SECRET = ""
    TRANSACTION_API_KEY = ""
    TRANSACTION_API_SECRET = ""

    # Mail
    MAIL_SERVER = 'smtp.sendgrid.net',
    MAIL_PORT = 465, #587
    MAIL_USE_SSL = True,
    MAIL_USERNAME = 'smtp-key',
    MAIL_PASSWORD = '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'


class DevelopmentConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'postgresql://paycast:l0v3c0d1ng@localhost/paycast'
    DEBUG = True
    TESTING = True
    APP_NAME = 'Paycast'
    SECRET_KEY = 'RgDUQTs4nzQAQRJf9kevRgDUQTs4nzQAQRJf9kev'
    JWT_EXPIRATION_DELTA = timedelta(days=30)
    JWT_AUTH_URL_RULE = '/api/v1/auth'
    JWT_AUTH_HEADER_PREFIX = 'JWT'
    SECURITY_REGISTERABLE = True
    SECURITY_REGISTER_URL = '/create_account'
    SECURITY_RECOVERABLE = True
    SECURITY_TRACKABLE = True
    SECURITY_CONFIRMABLE = True
    SECURITY_PASSWORD_HASH = 'pbkdf2_sha512'
    SECURITY_PASSWORD_SALT = 'RgDUQTs4nzQAQRJf9kevRgDUQTs4nzQAQRJf9kev'
    SECURITY_POST_LOGIN_VIEW = '/dashboard'
    SECURITY_POST_CONFIRM_VIEW = '/post_confirm_redirect'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SECURITY_EMAIL_SENDER = 'no-reply@localhost'
    SECURITY_POST_LOGIN_VIEW = '/dashboard'
    SECURITY_POST_RESET_VIEW = '/login'
    FRONTEND_URL = "http://paycast.io/"
    OAUTH_CREDENTIALS = {
        'facebook': {
            'id': '185436661892522',
            'secret': '82a09d542035ee5f10360846b706cb4a'
        },
        'twitter': {
            'id': 'sGHuFe9gdbcc9hzKYkENt3Je6',
            'secret': 'etGxvdXGegdyO1HFo2XfONEDYNK76QREMcPSHsH7n8l7j2umvJ'
        },
        'google': {
            'id': '552817426609-6rfk2dto43877j0rsa0bnh9d1o1m6esi.apps.googleusercontent.com',
            'secret': 'AwVHLZ6XFw9bMwp-diDJ1_wT'
        },
        'github': {
            'id': '51fe28c094d75307a61a',
            'secret': 'ce7689f6557178eacf9608599d984e0625f3c181'
        }
    }
    LANGUAGES = {
        'en': 'English',
    }
    UPLOADS_FOLDER = os.path.join(os.path.dirname(os.path.dirname(__file__)), "uploaded_documents")
    ID_FRONT_UPLOAD_FOLDER = os.path.join(os.path.dirname(os.path.abspath(__file__)), "uploaded_documents/id_front")
    ID_BACK_UPLOAD_FOLDER = os.path.join(os.path.dirname(os.path.abspath(__file__)), "uploaded_documents/id_back")
    CERTIFICATE_OF_INCORPORATION_UPLOAD_FOLDER = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                                              "uploaded_documents/incorporation_certificate")
    DIRECTORS_LETTER_UPLOAD_FOLDER = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                                  "uploaded_documents/directors_letter")
    PIN_CERTIFICATE_UPLOAD_FOLDER = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                                 "uploaded_documents/pin_certificate")
    AVATARS_FOLDER = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                                  "uploaded_documents/avatars")

    ALLOWED_EXTENSIONS = set(['pdf', 'png', 'jpg', 'jpeg', 'gif'])
    ALLOWED_AVATARS = set(['png', 'jpg', 'jpeg', 'gif'])
    SENDGRID_API_KEY = "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"
    SENDGRID_DEFAULT_FROM = "admin@paycsat.tech"

    # After password reset, Redirect to Dennis' login page?
    SECURITY_POST_RESET_VIEW = ''

    # After password change?
    SECURITY_POST_CHANGE_VIEW = ''

    # API configuration
    INTERNAL_API_KEY = ""
    INTERNAL_API_SECRET = ""
    BILLING_API_KEY = ""
    BILLING_API_SECRET = ""
    TRANSACTION_API_KEY = ""
    TRANSACTION_API_SECRET = ""

    # Mail
    MAIL_SERVER = 'smtp.sendgrid.net',
    MAIL_PORT = 465,  # 587
    MAIL_USE_SSL = True,
    MAIL_USERNAME = 'smtp-key',
    MAIL_PASSWORD = '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'


class TestingConfig(Config):
    SQLALCHEMY_DATABASE_URI = 'sqlite://'
    TESTING = True
