[![Build Status](https://travis-ci.org/graup/flask-restless-security.svg?branch=master)](https://travis-ci.org/graup/flask-restless-security)

This is Paycast's authentication module. It is based on [Flask](http://flask.pocoo.org/) framework + API using:

- [Flask-Restless](https://flask-restless.readthedocs.org/en/latest/) (API)
- [Flask-Security](https://pythonhosted.org/Flask-Security/) (Authentication)
- [Flask-Login]() () (User session management)
- [Flask-JWT](https://pythonhosted.org/Flask-JWT/) (API authentication)
- [Flask-Admin](http://flask-admin.readthedocs.org/en/latest/) (Admin views)
- [SQLAlchemy](http://www.sqlalchemy.org/) (ORM)
- [Rauth](https://github.com/litl/rauth) (Social Oauth - Facebook, Twitter, Google)
- [Flask-OAuthlib](https://flask-oauthlib.readthedocs.io/en/latest/) (Social Oauth - Github)

The module also has stubs for testing.

Setup
=====

- Create and activate a vitualenv
- Run `pip install -r requirements.txt`
- Start server using `python server.py`

**Website**

- Browser session access. Minimal at the moment. User dashboard functions could be added.

**Admin**

- Access admin at /admin

**API auth**

- POST /api/v1/auth {'username': '', 'password': ''}
- Returns JSON with {'access_token':''}  
- Then request from API using header 'Authorization: JWT $token'
- Social authentication is also supported(View [Endpoints Documentation](API.md))

**Tests**

- Run tests using `python test.py`